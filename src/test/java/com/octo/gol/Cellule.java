package com.octo.gol;

class Cellule {
    private boolean estVivant;
    private int nombreDeVoisinsVivant;

    public Cellule(boolean estVivant, int nombreDeVoisinsVivant) {
        this.estVivant = estVivant;
        this.nombreDeVoisinsVivant = nombreDeVoisinsVivant;
    }

    public void generationSuivante() {
        this.estVivant = peutResterVivante() || peutRevivre();

    }

    private boolean peutRevivre() {
        return !estVivant && (nombreDeVoisinsVivant == 3);
    }

    private boolean peutResterVivante() {
        return estVivant && ((nombreDeVoisinsVivant == 2) || (nombreDeVoisinsVivant == 3));
    }

    public boolean estVivante() {
        return this.estVivant;
    }

}
