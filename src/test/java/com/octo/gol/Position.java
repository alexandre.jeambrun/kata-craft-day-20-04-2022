package com.octo.gol;

public record Position(int x, int y) {
}
