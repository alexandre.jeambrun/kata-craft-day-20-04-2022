package com.octo.gol;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class GameOfLifeShould {
    //These rules, which compare the behavior of the automaton to real life,
    // can be condensed into the following:
    //
    //Any live cell with two or three live neighbours survives.
    //Any dead cell with three live neighbours becomes a live cell.
    //All other live cells die in the next generation.
    // Similarly, all other dead cells stay dead.



    @Test
    void une_cellule_peut_etre_vivante() {
        //Arrange
        Cellule cellule = new Cellule(true, 2);
        //Act
        //Assert
        assertTrue(cellule.estVivante());
    }

    @Test
    void quand_cellule_sans_voisins_vivant_alors_cellule_doit_mourir() {
        //Arrange
        Cellule celluleVivante = new Cellule(true, 0);
        //Act
        celluleVivante.generationSuivante();
        //Assert
        assertFalse(celluleVivante.estVivante());
    }


    @Test
    void une_cellule_vivante_avec_3_voisins_vivants_reste_vivante() {
        //Arrange
        var cellule = new Cellule(true, 3);

        //Act

        cellule.generationSuivante();
        //Assert
        Assertions.assertTrue(cellule.estVivante());
    }
    @Test
    void une_cellule_vivante_avec_2_voisins_vivants_reste_vivante() {
        //Arrange
        var cellule = new Cellule(true, 2);

        //Act

        cellule.generationSuivante();
        //Assert
        Assertions.assertTrue(cellule.estVivante());
    }

    @Test
    void une_cellule_vivante_avec_4_voisins_ou_plus_vivants_meure() {
        //Arrange
        var cellule = new Cellule(true, 4);

        //Act
        cellule.generationSuivante();

        //Assert
        Assertions.assertFalse(cellule.estVivante());
    }

    @Test
    void une_cellule_morte_avec_3_voisins_vivants_revit() {
        //Arrange
        var cellule = new Cellule(false, 3);

        //Act
        cellule.generationSuivante();

        //Assert
        Assertions.assertTrue(cellule.estVivante());
    }

    @Test
    void une_cellule_morte_avec_2_voisins_reste_morte() {
        //Arrange
        var cellule = new Cellule(false, 2);

        //Act
        cellule.generationSuivante();

        //Assert
        Assertions.assertFalse(cellule.estVivante());
    }


}
